;; dev.el -- no-byte-compile: t -- -*- lexical-binding: t; -*-

;; disable tabs for indentation
(setq-default indent-tabs-mode nil)

(setq c-default-style "k&r" ; set k&r style indentation in c
      compilation-scroll-output 'first-error) ; scroll to first error

;; add compilation keybinding to prog-mode
(define-key prog-mode-map (kbd "<f9>") 'compile)
(define-key prog-mode-map (kbd "S-<f9>") 'recompile)

;; bind find-references to s-.
(define-key global-map (kbd "s-.") 'xref-find-references)

;; enable automatic recompilation of elisp source files
(use-package auto-compile
  :demand t
  :config
  (auto-compile-on-load-mode)
  (auto-compile-on-save-mode))

;; enable company-mode for all programming modes
(use-package company
  :hook (prog-mode . company-mode))

;; enable dash-at-point to search word at point in Dash (kapeli.com)
(when (eq system-type 'darwin) ;; Dash is only on macOS
  (use-package dash-at-point
    :commands (dash-at-point)
    :bind ("s-d" . dash-at-point)))

;; enable display-line-numbers-mode in conf, yaml, and programming modes
(use-package display-line-numbers
  :hook
  (prog-mode . display-line-numbers-mode)
  (conf-mode . display-line-numbers-mode)
  (yaml-mode . display-line-numbers-mode)
  :config (setq display-line-numbers-type 'relative))

;; Docker modes
(use-package dockerfile-mode
  :mode ("Dockerfile.*\\'" . dockerfile-mode))
(use-package docker-compose-mode
  :mode ("docker-compose.*.yml\\'" . docker-compose-mode))

;; fish mode for fish scripts
(use-package fish-mode
  :mode ("\\.fish\\'" . fish-mode))

;; use flycheck for syntax checking
(use-package flycheck
  :hook (prog-mode . flycheck-mode))

;; golang mode
(use-package go-mode
  :mode ("\\.go\\'" . go-mode)
  :hook
  (go-mode . (lambda ()
               (add-hook 'before-save-hook #'lsp-format-buffer t t)
               (add-hook 'before-save-hook #'lsp-organize-imports t t))))

;; lsp mode
(use-package lsp-ui
  :hook
  (go-mode . lsp-deferred)
  (python-mode . lsp-deferred)
  (terraform-mode . lsp-deferred)
  (ruby-mode . lsp-deferred)
  (lsp-mode . lsp-enable-which-key-integration)
  :config
   (setq lsp-modeline-diagnostics-enable nil
         lsp-ui-sideline-show-code-actions t)
  :bind
  (:map lsp-ui-mode-map
        ("M-i" . lsp-ui-imenu)
        ([remap xref-find-definitions] . lsp-ui-peek-find-definitions)
        ([remap xref-find-references] . lsp-ui-peek-find-references)
        ([remap display-local-help] . lsp-ui-doc-toggle)
        ("C-h SPC" . lsp-ui-doc-focus-frame)))
(use-package lsp-ivy
  :commands lsp-ivy-workspace-symbol)

;; enable magit when called
(use-package magit
  :bind ("C-x g" . magit-status))

;; Nix Mode
(use-package nix-mode
  :mode "\\.nix\\'")

;; Powershell mode
(use-package powershell
  :mode ("\\.ps1\\'" . powershell-mode))

;; Projectile
(use-package counsel-projectile
  :bind
  (("s-p SPC" . counsel-projectile)
   :map projectile-mode-map
   ("s-p" . projectile-command-map))
  :hook (prog-mode . counsel-projectile-mode)
  :config
  (counsel-projectile-mode))

;; Puni mode
(use-package puni
  :defer t
  :init
  (puni-global-mode)
  (dolist (key '("C-w" "C-d" "DEL"))
    (unbind-key key puni-mode-map))
  :bind
  ("M-@" . puni-expand-region)
  ("C-M-t" . puni-transpose)
  ("M-<right>" . puni-slurp-forward)
  ("M-<left>" . puni-barf-forward)
  ("C-M-<left>" . puni-slurp-backward)
  ("C-M-<right>" . puni-barf-backward)
  :hook (term-mode . puni-disable-puni-mode))

;; Python mode
(use-package python)
(use-package pyvenv)
(use-package blacken
  :hook (python-mode . blacken-mode))

;; Enable rainbow-delimiters-mode in all programming-related modes
(use-package rainbow-delimiters
  :commands rainbow-delimiters-mode
  :hook (prog-mode . rainbow-delimiters-mode))

;; rustic for rust
(use-package rustic
  :defer t
  :config (setq rustic-format-on-save t))

;; terraform mode
(use-package terraform-mode
  :mode ("\\.tf\\'" . terraform-mode)
  :hook (terraform-mode . terraform-format-on-save-mode)
  :config (company-terraform-init))
(use-package company-terraform)

;; enable vagrant-tramp
(unless (eq system-type 'windows-nt) ; vagrant-tramp blows up on windows
    (use-package vagrant-tramp))

;; enable yaml-mode
(use-package yaml-mode
  :mode
  ("\\.ya?ml\\'" . yaml-mode)
  ("_.*\\.tpl\\'" . yaml-mode)
  ("\\.sls\\'" . yaml-mode )
  :hook
  (yaml-mode . flyspell-prog-mode)
  (yaml-mode . (lambda ()
                 (electric-quote-local-mode -1)
                 (visual-line-mode -1))))

;; many yasnippets completions
(use-package yasnippet-snippets
  :defer t
  :init (yas-global-mode))
