;; linux.el -- no-byte-compile: t -- -*- lexical-binding: t; -*-

;; ignore keypresses that are not for emacs
(dolist
    (key (list
          "<key-4096>"                  ; Used with x11-caps_enter_as_ctrl script
          "<key-4097>"                  ; Used with x11-caps_enter_as_ctrl script
          "<XF86AudioLowerVolume>"
          "<XF86AudioRaiseVolume>"
          "<XF86AudioPrev>"
          "<XF86AudioNext>"
          "<XF86AudioStop>"
          "<XF86AudioPlay>"))
  (global-set-key (kbd key) 'ignore))

;; add .local/bin to path
(let
    ((local_bin (concat (getenv "HOME") "/.local/bin")))

  (if (file-directory-p local_bin)
      (add-to-list 'exec-path local_bin)))

;; load common BSD/Linux/Mac settings
(load "nix_common")
