;; mac.el -- no-byte-compile: t -- -*- lexical-binding: t; -*-

(setq mac-option-modifier 'super ; set option to super
      ispell-program-name "enchant-2") ; set enchant as spell checker

;; pseudo daemon for multi-tty workaround
(use-package mac-pseudo-daemon
  :if (display-graphic-p)
  :init
  (mac-pseudo-daemon-mode)
  (server-start))

;; show menu bar on graphical windows, hide in terminal
(add-hook 'after-make-frame-functions
          #'(lambda (&optional frame)
              "Show FRAME (default: selected frame) menubar on graphical windows,
               hide menubar if in terminal"
              (set-frame-parameter frame 'menu-bar-lines
                                   (if (display-graphic-p frame)
                                       1 0))))

;; load common BSD/Linux/Mac settings
(load "nix_common")
