;; nix-common.el -- no-byte-compile: t -- -*- lexical-binding: t; -*-

;; set ssh as default tramp method
(setq tramp-default-method "ssh")
