;; org-conf.el -- no-byte-compile: t -- -*- lexical-binding: t; -*-

(setq org-hide-emphasis-markers t ; hide org emphasis markup
      org-pretty-entities t ; insert special symbols w/ LaTeX-like syntax
      org-use-sub-superscripts "{}" ; only super/subscript things in curly brackets
      org:zettelkasten-dir "~/zk/")

;; ob-go to evaluate golang code in org-mode
(use-package ob-go
  :config
  (add-to-list 'org-babel-load-languages '(go . t)))

;; org-mode settings
(use-package org
  ;; Bind counsel-org-goto
  :config
  (define-key org-mode-map (kbd "C-c C-j") 'counsel-org-goto)
  (define-key org-mode-map (kbd "C-u C-c C-j") 'counsel-org-goto-all))

;; toggle emphasis markers when the cursor is on a word
(use-package org-appear
  :hook (org-mode . org-appear-mode))

;; prettify org bullets
(use-package org-superstar
  :diminish
  :hook (org-mode . org-superstar-mode)
  :init
  (setq org-superstar-remove-leading-stars t
        org-superstar-headline-bullets-list '("⁖" "◉" "○" "✿" "✸")
        org-superstar-leading-bullet ?\s
        ;; Only use the first 4 styles and do not cycle.
        org-cycle-level-faces nil
        org-n-level-faces 4)
  ;; org headline sizes are modified in theme.el
  ;; so that they can be reloaded when the theme changes
  )

;; org-roam for zettelkasten
(when (file-directory-p org:zettelkasten-dir)
  (use-package org-roam
    :custom
    (org-roam-directory (file-truename org:zettelkasten-dir))
    (org-roam-dailies-directory "journals/")
    (org-roam-capture-templates '(("d" "default" plain "%?" :target
                                   (file+head "pages/%<%Y%m%d%H%M%S>-${slug}.org"
                                              "#+title: ${title}\n")
                                   :unnarrowed t)))
    :bind
    ("s-r l" . org-roam-buffer-toggle)
    ("s-r f" . org-roam-node-find)
    ("s-r g" . org-roam-graph)
    ("s-r i" . org-roam-node-insert)
    ("s-r c" . org-roam-capture)
    ("s-r t" . org-roam-tag-add)
    ("s-r r" . org-roam-ref-add)
    ;; Dailies
    ("s-r d c" . org-roam-dailies-capture-today)
    ("s-r d t" . org-roam-dailies-goto-today)
    ("s-r d y" . org-roam-dailies-goto-yesterday)
    ("s-r d d" . org-roam-dailies-goto-date)
    :init
    (which-key-add-key-based-replacements "s-r d" "org-roam dailies")
    :config
    ;; set tag column to 40% of window body width
    (setq org-roam-node-display-template #'(lambda ()
                                             (concat
                                              "${title:*} "
                                              (propertize
                                               (format "${tags:%d}"
                                                       (* (window-body-width) .4))
                                               'face
                                               'org-tag)))
          org-roam-mode-sections '((org-roam-backlinks-section :unique t)
                                   org-roam-reflinks-section
                                   org-roam-unlinked-references-section))
    (add-to-list 'org-roam-file-exclude-regexp
                 "logseq/bak")
    (add-to-list 'display-buffer-alist
                 '("\\*org-roam\\*"
                   (display-buffer-in-direction)
                   (direction . right)
                   (window-width . 0.33)
                   (window-height . fit-window-to-buffer)))
    (add-to-list 'org-cite-global-bibliography
                 (concat org:zettelkasten-dir "sources.bib"))
    (org-roam-db-autosync-mode)))

;; org-roam-ui
(when (file-directory-p org:zettelkasten-dir)
  (use-package org-roam-ui))
