;; ui.el -- no-byte-compile: t -- -*- lexical-binding: t; -*-

;; set variable pitch font size to 13 pt
(set-face-attribute 'variable-pitch nil :height 130)

;; hide menubar unless on a mac
(unless (eq system-type 'darwin)
  (menu-bar-mode -1))

;; hide scrollbar
(with-demoted-errors
 (scroll-bar-mode -1))

;; hide toolbar
(tool-bar-mode -1)

;; set frame width to 100 columns
(add-to-list 'default-frame-alist '(width . 100))

;; Set variable-pitch font for text modes
(add-hook 'text-mode-hook 'variable-pitch-mode)

;; increase default line spacing
(setq-default line-spacing 1)

(if (< (display-pixel-height) 1050)
    ;; maximize screen height if shorter than 1050px
    (add-to-list 'default-frame-alist '(fullscreen . fullheight))
  ;; otherwise set frame height to 69 rows
  (add-to-list 'default-frame-alist '(height . 69)))

;; enable mouse in terminal
(when (eq window-system nil)
  (xterm-mouse-mode t)
  (global-set-key (kbd "<mouse-4>") 'scroll-down-line)
  (global-set-key (kbd "<mouse-5>") 'scroll-up-line))

(defun set-theme (theme)
  "Set dark/light THEME based on argument."
  (let ((dark-theme 'doom-moonlight)
        (light-theme 'doom-flatwhite)
        (select-theme (lambda (old-theme new-theme)
                        (use-package doom-themes
                          :config
                          (disable-theme old-theme)
                          (load-theme new-theme t)))))
    ;; sets theme
    (cond
     ((eq theme 'light)
      ;; set light theme
      (funcall select-theme dark-theme light-theme))
     ((eq theme 'dark)
      ;; set dark theme
      (funcall select-theme light-theme dark-theme))))
  ;; flash the modeline for the bell
  (doom-themes-visual-bell-config)
  ;; set xterm colors
  (setq xterm-color-names
        ["#292d3e"   ; black
         "#ff5370"   ; red
         "#c7f59b"   ; green
         "#ffbd76"   ; yellow
         "#74a0f1"   ; blue
         "#baacff"   ; magenta
         "#33d3fb"   ; cyan
         "#dcebff"]  ; white
        xterm-color-names-bright
        ["#434758"   ; black
         "#ff6a6a"   ; red
         "#7af8ca"   ; green
         "#fff376"   ; yellow
         "#70b2ff"   ; blue
         "#f3c1ff"   ; magenta
         "#89ddff"   ; cyan
         "#e4f3fa"]) ; white
  (with-eval-after-load 'org
    (cl-mapc
     #'(lambda (face inheritence height)
         (set-face-attribute face nil :inherit inheritence :height height))
     ;; Headlines scaled the same as in LaTeX (3-\large, 2-\Large, 1-\LARGE)
     ;; Title scaled to LaTeX \huge
     '(org-level-3 org-level-2 org-level-1 org-document-title)
     '(outline-3 outline-2 outline-1 title)
     '(1.2 1.44 1.728 2.074))))

;; Set theme
(let ((default-theme 'dark)
      (mac-select-theme (lambda ()
                          (if ;; requires Mitsuharu Yamamoto's excellent mac port
                              (string= (plist-get (mac-application-state) :appearance)
                                       "NSAppearanceNameAqua")
                              (set-theme 'light)
                            (set-theme 'dark))))
      (linux-select-theme (lambda (values)
                            (let ((scheme (car values)))
                              (cond
                               ((eq 1 scheme) ;; color-scheme 1 is dark
                                (set-theme 'dark))
                               ((eq 2 scheme) ;; color-scheme 2 is light
                                (set-theme 'light))
                               (t (message "Unknown color-scheme id: %s" scheme)))))))
  (cond
   ((eq system-type 'darwin)
    ;; if running on mac, set theme to match appearance
    (funcall mac-select-theme)
    ;; change theme when mac theme changes
    (add-hook 'mac-effective-appearance-change-hook mac-select-theme))
   ((and (eq system-type 'gnu/linux) (progn
				       (require 'dbus)
				       (dbus-list-activatable-names)))
    ;; if running on linux, set theme to match appearance
    (require 'dbus)
    (let ((color-scheme (car
                         (dbus-call-method :session
                                           "org.freedesktop.portal.Desktop"
                                           "/org/freedesktop/portal/desktop"
                                           "org.freedesktop.portal.Settings"
                                           "Read" "org.freedesktop.appearance" "color-scheme"))))
      (funcall linux-select-theme color-scheme))
    ;; change theme when desktop theme changes
    (dbus-register-signal :session
                          "org.freedesktop.portal"
                          "/org/freedesktop/portal/desktop"
                          "org.freedesktop.impl.portal.Settings"
                          "SettingChanged"
                          (lambda (category setting values)
                            (when (string= setting "color-scheme")
                              (funcall linux-select-theme values)))))
    ;; if not running on mac or linux, set default-theme
   (t (set-theme default-theme))))

;; enabled mixed-pitch fonts
(use-package mixed-pitch
  :hook
  (text-mode . mixed-pitch-mode))

;; enable doom-modeline
(use-package doom-modeline
;; run `M-x nerd-icons-install-fonts' to make the ciper complete
  :init
  (doom-modeline-mode))

;; Distraction-free screen
(use-package olivetti
  :init
  (setq olivetti-body-width .33
        olivetti-minimum-body-width 60)
  :bind
  ("<f12>" . (lambda ()
               "Toggle distraction-free writing environment"
               (interactive)
               (if (equal olivetti-mode nil)
                   (progn
                     (window-configuration-to-register 1)
                     (delete-other-windows)
                     (text-scale-set 2)
                     (olivetti-mode))
                 (progn
                   (with-demoted-errors
                       (jump-to-register 1))
                   (set-register 1 nil)
                   (olivetti-mode -1)
                   (text-scale-set 0))))))

;; set F11 to toggle fullscreen
(global-set-key (kbd "<f11>")
                #'(lambda (&optional frame)
                    "Toggle fullscreen FRAME (default: selected frame)"
                    (interactive)
                    (set-frame-parameter frame 'fullscreen
                                         (let ((fullscreen-parameter (frame-parameter frame 'fullscreen)))
                                           (unless (or (eq fullscreen-parameter 'fullboth)
                                                       (eq fullscreen-parameter 'fullscreen))
                                             'fullscreen)))))

;; set S-F11 to toggle full height
(global-set-key (kbd "S-<f11>")
                #'(lambda (&optional frame)
                    "Toggle fullheight FRAME (default: selected frame)"
                    (interactive)
                    (set-frame-parameter frame 'fullscreen
                                         (unless (eq (frame-parameter frame 'fullscreen) 'fullheight)
                                           'fullheight))))
