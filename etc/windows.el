;; windows.el -- no-byte-compile: t -- -*- lexical-binding: t; -*-

(setq w32-pass-apps-to-system nil ; don't pass apps (menu) key to system
      w32-apps-modifier 'super ; set apps (menu) key to super
      tramp-use-ssh-controlmaster-options nil ; doesn’t work on windows
      tramp-default-method "sshx") ; works with windows builtin openSSH

;; set hunspell dictionary to ispell-local-dictionary
(setenv "DICTIONARY" ispell-local-dictionary)

;; add emacs bin dir to Emacs path
(let
    ((emacs_bin (concat (getenv "emacs_dir") "/bin")))
  (setenv "PATH" (concat (getenv "PATH")
                         (concat ";" emacs_bin)))
  (add-to-list 'exec-path emacs_bin))

;; load sqlite3 when using org-roam on windows
(when (file-directory-p org:zettelkasten-dir)
  (use-package emacsql-sqlite3
    :config (setq org-roam-database-connector 'sqlite3)))
