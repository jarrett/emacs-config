;; init.el -- no-byte-compile: t --  -*- lexical-binding: t; -*-

(setq apropos-sort-by-scores t ; sort apropos by relevance
      auto-hscroll-mode 'current-line ; horizontally scroll current line to keep point visible
      backup-by-copying t ; preserve backup file creation date>
      create-lockfiles nil ; disable lockfiles
      delete-by-moving-to-trash t ; move files to system trash when deleting
      delete-old-versions t ; delete old backups
      enable-recursive-minibuffers t ; allow multiple minibuffers
      ffap-machine-p-known 'reject ;prevent ffap from pinging and connecting to remote machines
      gc-cons-threshold 20000000 ; set garbage collection threshold to 200MB
      inhibit-startup-screen t ; hide startup screen ; don’t keep any old backup versions
      initial-scratch-message nil ; remove initial scratch message
      ispell-local-dictionary "en_US"
      ispell-local-dictionary-alist ; spellcheck settings
      '(("en_US" "[[:alpha:]]" "[^[:alpha:]]" "['’]" t nil nil utf-8))
      kept-new-versions 8 ; keep 8 new backups
      kept-old-versions 0; keep no old backups
      mouse-wheel-tilt-scroll t ; allow horizontal scrolling
      mouse-wheel-flip-direction t ; enable "natural" horizontal scrolling
      native-comp-async-report-warnings-errors 'silent ; silently log native-comp errors
      read-process-output-max (* 1024 1024) ; set data max data read from subprocess to 1MB
      require-final-newline t ; automatically add newline to the end of files
      ring-bell-function 'ignore ; no system bells please
      save-interprogram-paste-before-kill t ; save clipboard into kill ring before replacement
      sentence-end-double-space nil ; allow sentences to end with a single space
      vc-follow-symlinks t ; always follow symlinks to version controlled files
      version-control t) ; use versioned backups

(setq-default truncate-lines t) ; don't automatically wrap lines

;; prefer utf-8, unix line endings, and unicode
(prefer-coding-system 'utf-8-unix)
(set-charset-priority 'unicode)

;; replace selected text with typed text
(delete-selection-mode)

;; indicate minibuffer depth
(minibuffer-depth-indicate-mode)

;; enable right-click context menu
(with-demoted-errors ;; handling errors for versions < 28
    (context-menu-mode))

;; add etc dir to load-path
(add-to-list 'load-path (file-name-as-directory
                         (concat user-emacs-directory "etc")))

;; initialize packages
(load "packages")

;; no littering my .emacs.d dir please
(use-package no-littering
  :demand
  :config
  (setq auto-save-file-name-transforms
	`((".*" ,(no-littering-expand-var-file-name "auto-save/") t))
        custom-file (concat no-littering-etc-directory "custom.el")
        backup-directory-alist (list
                                ;; Don't backup potentially sensitive information
                                (cons "^/dev/shm/" nil)
                                (cons "^/tmp/"     nil)
                                (cons "."         (no-littering-expand-var-file-name "backup/")))))

;; load configs
(dolist (conf (list
               "org-conf"
               "dev"
               ;; platform specific settings
               (cond ((eq system-type 'darwin) "mac")
                     ((eq system-type 'gnu/linux) "linux")
                     ((eq system-type 'berkeley-unix) "bsd")
                     ((eq system-type 'windows-nt) "windows"))
               "ui"))
  (load conf))

;; handle file-errors when loading local because file may not always be there
(with-demoted-errors
    (load "local"))

(let ((backup-dir (car (last (flatten-list backup-directory-alist)))))
  ;; create backup dir if it doesn't exist
  (unless (file-directory-p backup-dir)
    (make-directory backup-dir))
  ;; delete backup files older than one week
  (let ((one-week 604800) ;; one week in seconds
        (current-time (float-time)))
    (dolist (file (directory-files backup-dir t))
      (when (and (backup-file-name-p file)
                 (> (- current-time (float-time (nth 5 (file-attributes file))))
                    one-week))
        (message "Removing old backup: %s" (file-name-nondirectory file))
        (delete-file file)))))

;; bind C-x C-S-c to save-buffers-kill-emacs
(define-key global-map (kbd "C-x C-S-c") 'save-buffers-kill-emacs)

;; set M-S-z to zap-up-to character
(global-set-key (kbd "M-Z") 'zap-up-to-char)

;; s-i - to insert text
(define-prefix-command 'my:insert-text)
(global-set-key (kbd "s-i") 'my:insert-text)
;; s-i d for short date MMDD
(define-key my:insert-text (kbd "d") (cons
                                      "Insert short date MMDD"
                                      (lambda()
                                       (interactive)
                                       (insert (format-time-string "%m%d")))))

;; select buffer for next error with s-g
(global-set-key (kbd "s-g") 'next-error-select-buffer)

;; electric config
(use-package electric
  :init
  (electric-pair-mode)
  (electric-quote-mode)
  ;; disable electric pairing for ‘<’ in org mode
  :hook (org-mode . (lambda ()
                      (setq-local electric-pair-inhibit-predicate
                                  `(lambda (c)
                                     (if (char-equal c ?<)
                                         t
                                       (,electric-pair-inhibit-predicate c))))))
  :config
  (setq electric-quote-replace-double t
        electric-quote-context-sensitive t
        electric-pair-inhibit-predicate 'electric-pair-conservative-inhibit)
  (add-to-list 'electric-quote-inhibit-functions 'org-in-src-block-p))

;; enable flymake-proselint
(when (executable-find "proselint")
  (use-package flymake-proselint
    :hook
    (text-mode . flycheck-mode)
    (text-mode . flymake-proselint-setup)))

;; enable flyspell in text and programming mode
(use-package flyspell
  :commands flyspell-mode
  :hook
  (text-mode . flyspell-mode)
  (prog-mode . flyspell-prog-mode))

;; enable hippie-expand
(use-package hippie-exp
  :commands hippie-expand
  :bind ("M-/" . hippie-expand))

;; enable ivy
(use-package counsel
  :config (ivy-mode)
  (setq ivy-use-virtual-buffers t
        ivy-count-format "(%d/%d)"
        ivy-initial-inputs-alist nil ;; remove leading ^ in inputs for quick substring matches
        ivy-use-selectable-prompt t
        ivy-re-builders-alist
        '((swiper-isearch . ivy--regex-plus)
          (t . ivy--regex-fuzzy)))
  :bind
  ("C-s"     . 'swiper-isearch)
  ("C-r"     . 'ivy-previous-line-or-history)
  ("M-x"     . 'counsel-M-x)
  ("M-i"     . 'counsel-imenu)
  ("C-x C-f" . 'counsel-find-file)
  ("C-h f"   . 'counsel-describe-function)
  ("C-h v"   . 'counsel-describe-variable)
  ("C-h l"   . 'counsel-find-library)
  ("C-h a"   . 'counsel-apropos)
  ("C-h S"   . 'counsel-info-lookup-symbol)
  ("C-h u"   . 'counsel-unicode-char)
  ("C-x l"   . 'counsel-locate)
  ("C-c C-r" . 'ivy-resume))
;; for ivy fuzzy matching ordering
(use-package flx
  :defer t)

;; markdown config
(use-package markdown-mode
  ;; use GitHub-flavored markdown mode for markdown files under git control
  :mode ("\\.md\\'" .(lambda ()
                       (if (eq 'Git (vc-backend (buffer-file-name)))
                           (gfm-mode)
                         (markdown-mode))))
  :init (setq markdown-command "multimarkdown"))

;; nov.el for epubs
(use-package nov
  :mode ("\\.epub\\'" . nov-mode)
  ;; set epub-mode font face to 14 pt
  :hook (nov-mode . (lambda ()
                      (face-remap-add-relative 'variable-pitch :height 140)
                      (nov-render-document))))

;; ripgrep
(use-package rg
  :init (rg-enable-menu))

;; Enable visual-line-mode
(use-package visual-line
  :ensure nil                           ;built in package
  :no-require t
  :init
  (with-current-buffer (get-buffer " *Echo Area 0*")
    (turn-on-visual-line-mode))
  :hook
  (text-mode . turn-on-visual-line-mode)
  (org-roam-mode . turn-on-visual-line-mode)
  (compilation-mode . turn-on-visual-line-mode))

;; which key
(use-package which-key
  :config (which-key-mode))

;; enable whole-line-or-region-mode
(use-package whole-line-or-region
  :config (whole-line-or-region-global-mode))

;; enable whitespace butler
(use-package ws-butler
  :hook
  (text-mode . ws-butler-mode)
  (prog-mode . ws-butler-mode))

;; enable windmove
(use-package windmove
  :init
  (windmove-default-keybindings)
  (windmove-swap-states-default-keybindings)
  :config (setq windmove-wrap-around t))
